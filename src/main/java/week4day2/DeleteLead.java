package week4day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week5day1.ProjectMethod;

public class DeleteLead extends ProjectMethod {
		
		//@BeforeClass(groups="common")
		@BeforeClass
		@DataProvider(name="fetchData")
		public void setData() {
				testcaseName = "TC01_DeleteLead";
				testcaseDesc = "Deleting a lead";
				author = "Gowri";
				category  = "regression";
				excelFilename = "Create Lead";
			}
		
		@Test(dataProvider="fetchData",dependsOnMethods="testcasesWithReportIntergration.CreateLeadwithReport.createLeadTC")
		public void deleting(String cunCode, String area, String phoneno) throws InterruptedException
		{
			//Launching browser and till clicking on Find Leads link...
			/*startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			type(elePassword, "crmsfa");
			WebElement elelogin = locateElement("classname","decorativeSubmit");
			click(elelogin);*/
			//doLogin();
			click(locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']"));
			click(locateElement("linktext", "Find Leads"));
			
			//clicking Phone tab...
			click(locateElement("xpath", "//*[text()='Phone']"));
			
			WebElement countryCode = locateElement("name", "phoneCountryCode");
			countryCode.clear();
			type(countryCode, cunCode);
			WebElement areaCode = locateElement("name", "phoneAreaCode");
			type(areaCode, area);
			WebElement phoneNumber = locateElement("name", "phoneNumber");
			type(phoneNumber, phoneno);
			
			click(locateElement("xpath", "//button[text()='Find Leads']"));
			Thread.sleep(3000);
			//Getting text of and clicking on the first resulting lead id...
			String firstLeadId = getText(locateElement("xpath", "//*[@class='x-grid3-body']/div[1]/table[1]/tbody/tr[1]/td[1]"));
			click(locateElement("xpath", "//*[@class='x-grid3-body']/div[1]/table[1]/tbody/tr[1]/td[1]//a"));
			
			//Clicking on Delete button on the next form...
			click(locateElement("linktext", "Delete"));
			
			//Going back to Find Leads and verifying the deletion...
			click(locateElement("linktext", "Find Leads"));
			type(locateElement("name", "id"), firstLeadId);
			click(locateElement("xpath", "//button[text()='Find Leads']"));
			
			//String message = getText(locateElement("xpath", "//*[@class='x-paging-info']"));
			verifyExactText(locateElement("xpath", "//*[@class='x-paging-info']"), "No records to display");
			
			//closeBrowser();
		}
		/*public String[][] getData()
		{
			String[][] data = new String[1][3];
			data[0][0]="91";
			data[0][1]="605";
			data[0][2]="8754732388";
			return data;
		}*/
}
