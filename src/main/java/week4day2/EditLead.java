package week4day2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week5day1.ProjectMethod;

public class EditLead extends ProjectMethod {

		//@Test(dependsOnMethods="week4day2.CreateLead1.create")
	@Test(groups = {"Sanity"})
		public void edit(){
			//loginprocedure();
			
			WebElement eleLinkText = locateElement("linktext", "CRM/SFA");
			click(eleLinkText);
			
			WebElement eleLead = locateElement("linktext", "Leads");
			click(eleLead);
			
			WebElement eleFindLd = locateElement("linktext", "Find Leads");
			click(eleFindLd);
			
			WebElement eleFirstLd = locateElement("xpath", "//*[@class='x-grid3-body']/div[1]/table[1]/tbody/tr[1]/td[1]//a");
			WebDriverWait wait = new WebDriverWait(driver, 10);
			click(eleFirstLd);
			//wait.until(ExpectedConditions.elementToBeClickable(eleFirstLd));
			
			
			WebElement eleEditBtn = locateElement("xpath", "//div[@class='x-panel-tc']/div[2]/a[3]");
			click(eleEditBtn);
			
			WebElement eleUpdateCom = locateElement ("id", "updateLeadForm_companyName");
			eleUpdateCom.clear();
			type(eleUpdateCom, "Paypal");
			
			WebElement eleUpdateButton = locateElement("classname", "smallSubmit");
			click(eleUpdateButton);
			
			WebElement eleVerComName = locateElement("id", "viewLead_companyName_sp");
			verifyExactText(eleVerComName, "Paypal");
			System.out.println("The Element"+eleVerComName+"is verified Successfully");
			


	}

		
}
