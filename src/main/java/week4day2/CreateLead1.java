package week4day2;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week5day1.ProjectMethod;

public class CreateLead1 extends ProjectMethod{
	//@Test(invocationCount =2)
	@Test (groups= {"smoke"})
	public void create(){
		//loginprocedure();
		
		WebElement eleLinkText = locateElement("linktext", "CRM/SFA");
		click(eleLinkText);
		
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleComName = locateElement ("id", "createLeadForm_companyName");
		type(eleComName, "Xybion Corporation");
		
		WebElement eleFirName = locateElement("id", "createLeadForm_firstName");
		type(eleFirName, "Hamshwini");
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "Vignesh");
		
		WebElement eleFlocname = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFlocname, "Hamsi");
		
		WebElement elellocname = locateElement("id", "createLeadForm_lastNameLocal");
		type(elellocname, "Vicky");
		
		WebElement eleSalute = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalute, "Ms.");
		
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		Select sc = new Select(eleSource); 
		sc.selectByVisibleText("Direct Mail");
		//selectDropDownUsingText(eleSource, "Direct Mail");
		
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleTitle, "Sr. Tester");
		
		WebElement eleAnnRev = locateElement("id", "createLeadForm_annualRevenue");
		type(eleAnnRev, "7,00,000");
		
		WebElement eleIndus = locateElement("id", "createLeadForm_industryEnumId");
		Select ind = new Select(eleIndus);
		ind.selectByIndex(3);
		//selectDropDownUsingIndex(eleIndus, 3);
		
		WebElement eleOwner = locateElement("id", "createLeadForm_ownershipEnumId");
		int count = getdropdownCount(eleOwner);
		selectDropDownUsingIndex(eleOwner, count-2);
		
		WebElement eleSic = locateElement("id", "createLeadForm_sicCode");
		type(eleSic, "SIC001");
		
		WebElement eleDesc = locateElement("id", "createLeadForm_description");
		type(eleDesc, "Alert is a small message box which displays on-screen notification to give the user some kind of information or ask for permission to perform certain kind of operation. It may be also used for warning purpose.");
		
		WebElement eleINote = locateElement("id", "createLeadForm_importantNote");
		type(eleINote, "Alert is a small message box which displays on-screen notification to give the user some kind of information.");
		
		WebElement eleMCampaign = locateElement("id", "createLeadForm_marketingCampaignId");
		Select mc = new Select(eleMCampaign);
		mc.selectByValue("CATRQ_CAMPAIGNS");
		//selectDropdownUsingValue(eleMCampaign, "CATRQ_CAMPAIGNS");
		
		WebElement eleDOB = locateElement("id", "createLeadForm_birthDate" );
		type(eleDOB, "12/29/87");
				
		WebElement eleCcode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		eleCcode.clear();
		type(eleCcode, "5");
		
		WebElement eleAcode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(eleAcode, "12");
		
		WebElement elePhone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(elePhone, "9962237038");
		
		WebElement eleEmail = locateElement("id", "createLeadForm_primaryEmail");
		type(eleEmail, "ashpriyam_29@yahoo.com");		
		
		WebElement eleExt = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(eleExt, "091");
		
		WebElement eleDept = locateElement("id", "createLeadForm_departmentName");
		type(eleDept, "Testing");
		
		WebElement eleCurr = locateElement("id", "createLeadForm_currencyUomId");
		Select cur = new Select(eleCurr);
		cur.selectByValue("INR");
		
		WebElement eleEmp = locateElement("id", "createLeadForm_numberEmployees");
		type(eleEmp, "100");
		
		WebElement eleTick = locateElement("id", "createLeadForm_tickerSymbol");
		type(eleTick, "~");
		
		WebElement elePerson = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(elePerson, "AShok");
		
		WebElement eleWeb = locateElement("id", "createLeadForm_primaryWebUrl");
		type(eleWeb, "http://leaftaps.com/crmsfa/control/createLeadForm");
		
		WebElement eleToName = locateElement("id", "createLeadForm_generalToName");
		type(eleToName, "Ashwini Priya");
		
		WebElement eleAdd1 = locateElement("id", "createLeadForm_generalAddress1");
		type(eleAdd1, "3A, Nahar Abode Apts, Plot no 10");
		
		WebElement eleAdd2 = locateElement("id", "createLeadForm_generalAddress2");
		type(eleAdd2, "Jeychandran Nagar 3rd main road, Medavakkam");
		
		WebElement eleCity = locateElement("id", "createLeadForm_generalCity");
		type(eleCity, "Chennai");
		
		WebElement eleCountry = locateElement("id", "createLeadForm_generalCountryGeoId");
		Select ctry = new Select(eleCountry);
		ctry.selectByVisibleText("India");
		
		WebElement eleState = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		Select state = new Select(eleState);
		state.selectByVisibleText("TAMILNADU");
				
		WebElement elezip = locateElement("id", "createLeadForm_generalPostalCode");
		type(elezip, "600100");
				
		WebElement eleCreateButton = locateElement("classname", "smallSubmit");
		click(eleCreateButton);
		
		WebElement eleveryfname = locateElement("id", "viewLead_firstName_sp");
		verifyDisplayed(eleveryfname);
		
		
	}
	
	
}


