package week4day2;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import week5day1.ProjectMethod;

public class MergeLead1 extends ProjectMethod {

	@Test(groups = {"Regression"})
	public void merge() throws InterruptedException{
		WebElement elelink = locateElement("linktext","CRM/SFA");
		click(elelink);
		WebElement elecreatelead = locateElement("linktext","Create Lead");
		click(elecreatelead);
		WebElement elefindlead = locateElement("linktext","Merge Leads");
		click(elefindlead);
		WebElement elefromlead = locateElement("xpath","//img[@src='/images/fieldlookup.gif'][@alt='Lookup'][1]");
		click(elefromlead);
		switchToWindow(1);
		System.out.println(driver.getTitle());
		Thread.sleep(2000);
		driver.getTitle();
		WebElement eleleadid = locateElement("xpath","//input[@type='text'][@name='id']");
		type(eleleadid,"10059");
		//input[@type='text'][@class='x-form-text x-form-field']//
		WebElement eleflead = locateElement("xpath","//button[text()='Find Leads']");
		click(eleflead);
		Thread.sleep(2000);
		WebElement elefirstlead = locateElement("xpath","(//a[@class='linktext'])[1]");
		click(elefirstlead);
		switchToWindow(0);
		System.out.println(driver.getTitle());
		WebElement eletoLead= locateElement("xpath","//input[@id='partyIdTo']/following::a");
		click(eletoLead);
		switchToWindow(1);
		
		Thread.sleep(2000);
		driver.getTitle();
		WebElement eleleadid1 = locateElement("xpath","//input[@type='text'][@name='id']");
		type(eleleadid1,"10137");
		//input[@type='text'][@class='x-form-text x-form-field']//
		WebElement eleflead1 = locateElement("xpath","//button[text()='Find Leads']");
		click(eleflead);
		Thread.sleep(2000);
		WebElement elefirstlead1 = locateElement("xpath","(//a[@class='linktext'])[1]");
		click(elefirstlead1);
		switchToWindow(0);
		WebElement elefindleads = locateElement("linktext","Merge");
		click(elefindleads);
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		WebElement eleflead2 = locateElement("xpath","//a[text()='Merge Leads']");
		click(eleflead2);
		
		
		WebElement elefindleads1 = locateElement("linktext","Merge");
		click(elefindleads1);
		
		driver.switchTo().alert().accept();
		
		/*Thread.sleep(2000);
		WebElement eleleadid2 = locateElement("xpath","//input[@type='text'][@name='id']");
		type(eleleadid2,"10009");*/
		
		//WebElement eleflead3 = locateElement("xpath","//ul[@class='errorList']/li");
		//verifyTitle(eleflead3,"The following required parameter is missing: [opentaps.mergeParties.partyIdFrom]");
		//driver.close();
		

		
	
		
		
		
		
	}
	


}
