package week5day1;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicTestReport{
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	public String testcaseName, testcaseDesc, author, category;
	public static String excelFilename;
	@BeforeSuite
	public void startResult() {
		html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	@BeforeMethod
	public void startTestcase(){
		test = extent.createTest(testcaseName, testcaseDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	public void logStep(String desc, String status) {
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("FAIL")) {
			test.fail(desc);
		}else if(status.equalsIgnoreCase("WARN")) {
			test.warning(desc);
		}
	}
	@Test
	public void runReport() throws IOException {
		ExtentTest test = extent.createTest("Login", "Login top leftap");
		test.pass("username is entered", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		//test.fail("password not entered", 
				//MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.assignAuthor("Ashwini");
		test.assignCategory("Smoke");
	}
	@AfterSuite
	public void endResult() {
			extent.flush();
		
	}
	}
	
	

