package week5day1;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Excel.LearnExcel;
import wdMethods.SeMethods;

public class ProjectMethod extends SeMethods{
	@DataProvider(name = "fetchData")
	public static Object[] getData() throws IOException {
		return LearnExcel.excelData(excelFilename);
	}
	@Parameters({"browser","url","uname","pwd"})
	@BeforeMethod (groups = {"Common"})
	public void loginprocedure(String browserName, String URL, String username, String password){
		//startApp("chrome","http://leaftaps.com/opentaps");
		startApp(browserName,URL);
		WebElement eleUserName = locateElement("id", "username");
		//type(eleUserName, "DemoSalesManager");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		//type(elePassword, "crmsfa");
		type(elePassword, password);
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
	}
	@AfterMethod (groups = {"Common"})
	public void closeApp() {
		closeBrowser();
	}
	

}
