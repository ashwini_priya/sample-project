package week4day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {
	public static void main(String[] args) throws InterruptedException {
		// To execute the Merge Leads Test Case
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		//Click Merge Leads
		
		//For From Lead Set
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		//To Move to New Window
		Set<String> allwindowsFrom = driver.getWindowHandles();
		List<String> lstFrom = new ArrayList<>();
		lstFrom.addAll(allwindowsFrom);
		driver.switchTo().window(lstFrom.get(1));		
	// 	ByFirstName		
	//	driver.findElementByXPath("//input[@name='firstName']").sendKeys("Shankar");
		driver.findElementByXPath("//input[@name='id']").sendKeys("10121");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lstFrom.get(0));
		
		//For To Lead Set
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> allwindowsTo = driver.getWindowHandles();
		List<String> lstTo = new ArrayList<>();
		lstTo.addAll(allwindowsTo);
		driver.switchTo().window(lstTo.get(1));		
	
		driver.findElementByXPath("//input[@name='id']").sendKeys("10121");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lstTo.get(0));
		driver.findElementByXPath("//a[text()='Merge']").click();
		driver.switchTo().alert().accept();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
	}

}
