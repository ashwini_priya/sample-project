package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import week5day1.BasicTestReport;

public class SeMethods extends BasicTestReport implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./driver/geckodrivers.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
			} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			System.out.println("The Browser "+browser+" not Launched Successfully");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "classname": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("Element not found");
		} catch (WebDriverException e) {
			// TODO: handle exception
			System.err.println("Unknown exception occured");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {

		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The Data "+data+" Entered Successfully");
			takeSnap();
			}catch(Exception e)
			{
				System.out.println("type failed");
				takeSnap();
				
			}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" is clicked Successfully");
			takeSnap();
			}catch(Exception e)
			{
				System.out.println("click failed");
				takeSnap();
	}
	}
	public void clickwithoutSnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" is clicked Successfully");
			}catch(Exception e)
			{
				System.out.println("The Element"+ele+" not clicked");
	}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String textVal = ele.getText();
		return textVal;
	}
	
	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}


	public void selectDropDownUsingText(WebElement ele, String type, String data) {
		Select sc= new Select(ele);
		if(type.equals("Visible")) {
			try {
				sc.selectByVisibleText(data);
			} catch (WebDriverException e) {
				// TODO Auto-generated catch block
				System.err.println("selectbytext failed");
			}
		} else if(type.equalsIgnoreCase("value")) {
			try {
				sc.selectByValue(data);
			} catch (WebDriverException e) {
				// TODO Auto-generated catch block
				System.err.println("selectbyvalue failed");
			}
		} else if(type.equalsIgnoreCase("index")) {
			try {
				sc.selectByIndex(Integer.parseInt(data));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.err.println("selectbyindex failed");
			}
		}


	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void selectDropdownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}
	
	
	public int getdropdownCount(WebElement ele) {
		Select sc = new Select(ele);
		List<WebElement> options = sc.getOptions();
		int size = options.size();
		return size;
	}
	

	@Override
	public boolean verifyTitle(WebElement ele, String expectedTitle) {
		// TODO Auto-generated method stub
		try {
			String firstname = ele.getText();
			System.out.println(firstname);
			if(firstname.contentEquals(expectedTitle))
			System.out.println("Matched");
			else
			System.out.println("Not Matched");
			}
			catch (Exception e)
			{
				System.out.println("No such element");
				e.printStackTrace();
			}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		System.out.println("The Element "+ele+" is verified Successfully");

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		System.out.println("The Element "+ele+" is verified Successfully");

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		System.out.println(lst.size());		
		driver.switchTo().window(lst.get(index));		

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().getText();
		return null;
	}
	
	public void clear() {
		
	}
	
	/*public void getData() {
	}*/

	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.close();

	}
	





}
