package week3day1;

import java.util.List;

import javax.xml.transform.Source;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//setpath
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		//Object creation
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//passurl
		driver.get("http://leaftaps.com/opentaps/");
		//enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter password
		driver.findElementById("password").sendKeys("crmsfa");
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on crmsfa link
		driver.findElementByLinkText("CRM/SFA").click();
		//click on Create lead link
		driver.findElementByLinkText("Create Lead").click();
		//enter mandatory fields
		driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Hamsika");
		driver.findElementById("createLeadForm_lastName").sendKeys("Vignesh");
		//Enter non-mandatory field
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select(source); 
		sc.selectByVisibleText("Public Relations");
		WebElement campaign = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mc = new Select(campaign); 
		mc.selectByValue("CATRQ_CARNDRIVER");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select ind = new Select(industry); 
		//ind.selectByIndex(14);//Entering the specific count
		List<WebElement> allOptions = ind.getOptions();
		int cnt = allOptions.size();
		ind.selectByIndex(cnt - 3);
		for (int i=0; i<allOptions.size(); i++) {
			System.out.println(allOptions.get(i).getText());
		}
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Hamsi");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Vicky");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Ms");
		driver.findElementById("createLeadForm_birthDate").sendKeys("11/22/1989");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Lead");
		//click create lead 
		driver.findElementByClassName("smallSubmit").click();
		
		}

}
