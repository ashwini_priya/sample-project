package week5day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import week5day1.ProjectMethod;

public class CreateAccount extends ProjectMethod {
	@Test
	public void createaccount() throws InterruptedException {
		WebElement elelink = locateElement("linktext","CRM/SFA");
		click(elelink);
		WebElement eleAccount = locateElement("linktext","Accounts");
		click(eleAccount);
		WebElement eleCreateAcc = locateElement("linktext", "Create Account");
		click(eleCreateAcc);
		WebElement eleName = locateElement("id", "accountName");
		type(eleName, "Hamsika Vignesh A");
		
		WebElement eleDrp = locateElement("name", "industryEnumId");
		selectDropDownUsingText(eleDrp, "Distribution");
		
		WebElement eleCur = locateElement("id", "currencyUomId");
		Select cur = new Select(eleCur);
		cur.selectByVisibleText("INR - Indian Rupee");
		
		WebElement eleSrc = locateElement("id", "dataSourceId");
		Select src = new Select(eleSrc);
		src.selectByIndex(3);
		
		WebElement eleMar = locateElement("id", "marketingCampaignId");
		Select mar = new Select(eleMar);
		mar.selectByIndex(4);
		
		WebElement eleNum = locateElement("id", "primaryPhoneNumber");
		type(eleNum, "9962237038");
		
		WebElement eleCity = locateElement("id", "generalCity");
		type(eleCity, "Chennai");
		
		WebElement eleEmail = locateElement("id", "primaryEmail");
		type(eleEmail, "ashpriyam29@gmail.com");
		
		WebElement eleCntry = locateElement("id", "generalCountryGeoId");
		Select cntry = new Select(eleCntry);
		cntry.selectByVisibleText("India");
		WebElement eleState = locateElement("id", "generalStateProvinceGeoId");
		Select state = new Select(eleState);
		state.selectByVisibleText("TAMILNADU");
		
		WebElement eleSubmit = locateElement("classname", "smallSubmit");
		click(eleSubmit);
		
		WebElement eleCapAcc = locateElement("xpath", "(//span[@class=\"tabletext\"])[3]");
		String text = getText(eleCapAcc);
		String[] str = text.split("[\\(\\)]");
		System.out.println(str[0]);
		System.out.println(str[1]);
		
		WebElement eleFindAccount = locateElement("linktext","Find Accounts");
		click(eleFindAccount);

		WebElement eleAccID = locateElement("name", "id");		
		type(eleAccID, str[1]);	
		
		WebElement eleAccName = locateElement("xpath", "(//input[@name = 'accountName'])[2]");
		//(//input[@name = 'accountName'])[2]
		type(eleAccName, str[0]);
		
		WebElement eleFindAccBtn = locateElement("xpath", "//button[text() = 'Find Accounts']");
		click(eleFindAccBtn);
		Thread.sleep(2000);
		
		WebElement eleClickFirstID = locateElement("xpath", "(//a[@class = 'linktext'])[4]");
		click(eleClickFirstID);
		
		WebElement eleVerifyName = locateElement("xpath", "(//span[@class = 'tabletext'])[3]");
		verifyExactText(eleVerifyName, text);
		System.out.println("The Element "+text+" is verified Successfully");
		

	}

}
