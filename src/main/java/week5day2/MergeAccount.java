package week5day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week5day1.ProjectMethod;

public class MergeAccount extends ProjectMethod {
	@Test
	public void mergeacc() throws InterruptedException {
		//loginprocedure();
		WebElement elelink = locateElement("linktext","CRM/SFA");
		click(elelink);
		
		WebElement eleAccount = locateElement("linktext","Accounts");
		click(eleAccount);
		
		WebElement eleMergeAccount = locateElement("linktext","Merge Accounts");
		click(eleMergeAccount);
		
		WebElement eleFromAcc = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(eleFromAcc);
		switchToWindow(1);
		
		WebElement eleFrmAccID = locateElement("xpath", "//input[@name = 'id']");
		type(eleFrmAccID, "10598");
		
		WebElement eleFindAcc = locateElement("xpath", "(//td[@class = 'x-btn-center'])[1]");
		Thread.sleep(2000);
		click(eleFindAcc);
		
		WebElement eleFirstAccID = locateElement("xpath", "(//a[@class = 'linktext'])[1]");
		clickwithoutSnap(eleFirstAccID);
		switchToWindow(0);
		
		WebElement eleToAcc = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(eleToAcc);
		switchToWindow(1);
		
		WebElement eleToAccID = locateElement("xpath", "//input[@name = 'id']");
		type(eleToAccID, "10600");
		
		WebElement eleToFindAccs = locateElement("xpath", "(//button[@class = 'x-btn-text'])[1]");
		Thread.sleep(2000);
		click(eleToFindAccs);
		
		WebElement eleResAccID = locateElement("xpath", "(//a[@class = 'linktext'])[1]");
		clickwithoutSnap(eleResAccID);
		switchToWindow(0);
		
		WebElement eleMergeButton = locateElement("linktext","Merge");
		clickwithoutSnap(eleMergeButton);
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		Thread.sleep(2000);
		
		WebElement eleFindAccount = locateElement("linktext","Find Accounts");
		click(eleFindAccount);
		
		WebElement eleFindAccID = locateElement("name", "id");
		type(eleFindAccID, "10598");
		
		WebElement eleFindAccButton = locateElement("linktext","Find Accounts");
		click(eleFindAccButton);
		Thread.sleep(2000);
		
		WebElement eleVerifyMsg = locateElement("classname", "x-paging-info");
		verifyDisplayed(eleVerifyMsg);
		System.out.println("The Element "+eleVerifyMsg+" is verified Successfully");
		
		
		
	}
	

}
