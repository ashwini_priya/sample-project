package projectDay;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class SelectMobile {
	@Test
	//public static void main(String[] args) throws InterruptedException {
	public void flipkartMobile() throws InterruptedException{
		// TODO Auto-generated method stub
		//setpath
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		//Object creation
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//passurl
		driver.get("https://www.flipkart.com/");
		//close the login screen pop-uo
		driver.findElementByXPath("//button[text()='✕']").click();
		//select the Electronics
		driver.findElementByXPath("//span[text() = 'Electronics']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[text()='Mi'])[1]").click();
		//Verify the title of the selected option
		if(driver.getTitle().contains("Mi Mobiles")) {
			System.out.println("Mi Mobiles title verified");
		}
		else {
			System.out.println("Title mismatch");
		}
		//Get the Product details and Prices
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(5000);
		List<WebElement> allMobiles = driver.findElementsByClassName("_3wU53n");
		for (WebElement eachMobile : allMobiles) {
			System.out.println(eachMobile.getText());
		}
		List<WebElement> mobilePrice = driver.findElementsByXPath("//*[@class='_1vC4OE _2rQ-NK']");
		for (WebElement eachPrice: mobilePrice) {
			System.out.println(eachPrice.getText());
		}
		//Select the First item
		driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
		Set<String> allWindowsFrom = driver.getWindowHandles();
		List<String> lstFrom = new ArrayList<>();
		lstFrom.addAll(allWindowsFrom);
		driver.switchTo().window(lstFrom.get(1));
		//Verify the title of the selected option
		String text = driver.findElementByXPath("//span[@class='_35KyD6']").getText();
		System.out.println("The title "+text+" verified");
		//Review and Ratings count
		String ratingCount = driver.findElementByXPath("//span[text()[contains(.,'Ratings')]]").getText();
		System.out.println(ratingCount);
		String reviewCount = driver.findElementByXPath("//span[text()[contains(.,'Reviews')]]").getText();
		System.out.println(reviewCount);
		//Close browser
		driver.quit();	

}
}
