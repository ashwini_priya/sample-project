package Excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	//public static  void main(String[] args) throws IOException {
	public static Object[][] excelData(String filename) throws IOException {
		
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./Data/"+filename+".xlsx");
		//XSSFWorkbook wbook = new XSSFWorkbook("./Data\\Create Lead.xlsx");
		XSSFSheet sheet = wbook.getSheet("Create Lead");
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row Count:" +rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count:" +columnCount);
		Object[][] data = new Object[rowCount][columnCount];
		for (int i = 1; i <= rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				data[i-1][j] = stringCellValue;				
				System.out.println(stringCellValue);
				
			} 
		}
		wbook.close();
		return data;
	}

}
