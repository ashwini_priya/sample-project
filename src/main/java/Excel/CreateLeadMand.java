package Excel;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week5day1.ProjectMethod;

public class CreateLeadMand extends ProjectMethod {
	@BeforeClass (groups = {"common"})
	public void setData() {
			testcaseName = "TC01_Create Lead";
			testcaseDesc = "Create a lead";
			author = "Ashwini";
			category  = "smoke";
			excelFilename = "Create Lead - sample";
		}
	//@Test (groups= {"smoke"})
	@Test(dataProvider = "fetchData")
	public void create(String cname, String fname, String lname){
		
		WebElement eleLinkText = locateElement("linktext", "CRM/SFA");
		click(eleLinkText);
		
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleComName = locateElement ("id", "createLeadForm_companyName");
		type(eleComName, cname);
		
		WebElement eleFirName = locateElement("id", "createLeadForm_firstName");
		type(eleFirName, fname);
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lname);
		
		WebElement eleCreateButton = locateElement("classname", "smallSubmit");
		click(eleCreateButton);
		
		WebElement eleveryname = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleveryname, fname);
		System.out.println("The Element "+fname+" is verified Successfully");
	}
}